<?php
use App\Billing;
use App\Company;
use App\Customer;
use App\Date;
use App\Domain;
use App\Project;
use App\Risk;
use App\Tariff;
use App\Task;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->line('<fg=red;bg=white>Please Wait , we populate the database</>');
        $output = new ConsoleOutput();
        $progress = new ProgressBar($output, 30);
        $progress->start();

        // Create a default user
        DB::table('users')->insert([
            'name' => 'admin',
            'lastname' => 'admin',
            'phone' => '0612345678',
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret'),
        ]);
        $progress->advance();
        $this->command->info(' Creating Default User named : admin with password : secret');

        factory(User::class, 10)->create()->each(function ($user) use ($progress) {
            $progress->advance();
            $this->command->info(" Creating User named : $user->name ");
        });

        // Create a défault Company
        factory(Company::class, 1)->create()->each(function ($company) use ($progress) {
            $progress->advance();
            $this->command->info(" Creating Default Company named : $company->name");
        });

        for ($i=1; $i <= 4; $i++) {
            $tariff = factory(Tariff::class, 1)->create(['company_id' => 1, 'technical_level' => $i]);
            $progress->advance();
            $this->command->info("Creating Default Tariff for : " . $tariff->first()->company->name .  " with a level of " . $tariff->first()->technical_level);
        }

        $date = factory(Date::class)->create();
        $progress->advance();
        $this->command->info(" Creating Default Date ");

        // Create a défault Project
        factory(Project::class, 1)->create()->each(function ($project) use ($progress) {
            $progress->advance();
            $this->command->info(" Creating Default Project named : $project->name");
        });

        // Create a default customer
        factory(Customer::class, 1)->create()->each(function ($customer) use ($progress) {
            $progress->advance();
            $this->command->info(" Creating Default Customer named : $customer->name ");
        });

        factory(Date::class, 2)->create()->each(function ($date) use ($progress) {
            $progress->advance();
            $this->command->info(" Creating Default Date named : $date->start_date");
        });

        // Create defaults Tasks
        factory(Task::class, 2)->create()->each(function ($task) use ($progress) {
            $progress->advance();
            $this->command->info(" Creating Default Task named : $task->name");
        });

        // Create defaults Risks
        factory(Task::class, 2)->create()->each(function ($task) use ($progress) {
            factory(Risk::class, 2)->create([ 'task_id' => $task->id ])->each(function ($risk) use ($progress) {
                $progress->advance();
                $this->command->info(" Creating Default Risk named : $risk->name");
            });
        });

        $domain = factory(Domain::class)->create();
        $progress->advance();
        $this->command->info(" Creating Default Domain named : " . $domain->name);

        factory(Billing::class, 2)->create()->each(function ($billing) use ($progress) {
            $progress->advance();
            $this->command->info(" Creating Default Billing named : $billing->project_name");
        });

        $this->command->line('<fg=red;bg=white>The Database was populated !');
        Artisan::call('passport:install', ["--force"=> true ]);
        $this->command->line('<fg=red;bg=white>The Api client keys are generated !');
        $this->command->line('<fg=red;bg=white>Application is ready to work !');
        $this->command->line('<fg=red;bg=white>                             ');
        $this->command->line('<fg=red;bg=white> (҂⌣̀_⌣́)        (╯°□°）╯︵ ⌨  ');
        $this->command->line('<fg=red;bg=white>                             ');
        $this->command->line('<fg=red;bg=white> (҂⌣̀_⌣́)        ⌨ ︵╰(°□°╰）  ');
        $this->command->line('<fg=red;bg=white>                             ');
        $this->command->line('<fg=red;bg=white>    (╯°Д°）╯︵/(.□ . )       ');
        $this->command->line('<fg=red;bg=white>                             ');
    }
}
