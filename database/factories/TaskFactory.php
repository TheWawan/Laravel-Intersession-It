<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'name'              => $faker->name,
        'description'       => $faker->sentence,
        'technical_level'   => 1,
        'project_id'        => 1,
        'user_id'           => 1
    ];
});
