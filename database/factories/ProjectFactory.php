<?php

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'name'          =>  'Project-' . random_int(1, 100),
        'budget'        => $faker->randomFloat(2, 1, 10000),
        'description'   => $faker->sentence,

        'company_id'    => 1,
        'user_id'       => 1
    ];
});
