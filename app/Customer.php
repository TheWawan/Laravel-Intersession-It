<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
      'name',
      'email',
      'phone',
      'siret'
    ];

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function bills()
    {
      return Billing::where('customer_id', $this->id);
    }
}
