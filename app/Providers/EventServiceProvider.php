<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],

        'App\Events\ProjectBillingEvent' => [
          'App\Listeners\ProjectBillingListener',
        ],

        'App\Events\CustomerBillingEvent' => [
          'App\Listeners\CustomerBillingListener',
        ],

        'App\Events\CompanyBillingEvent' => [
          'App\Listeners\CompanyBillingListener',
        ],

        'App\Events\TariffModifEvent' => [
          'App\Listeners\PriceRecomputationListener',
        ], 

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
