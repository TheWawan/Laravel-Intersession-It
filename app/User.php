<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'lastname', 'phone', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */

    public function tariff()
    {
        return $this->hasOne('App\Tariff');
    }

    /*
    -------------------------------------------------------------
      Relation to the project.
    -------------------------------------------------------------
    */
    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    public function hasProjects()
    {
        return $this->projects()->exists();
    }

    public function participatingProjects()
    {
        return $this->belongsToMany('App\Project');
    }

    /*
    -------------------------------------------------------------
      Relation to the company.
    -------------------------------------------------------------
    */
    public function companies()
    {
        return $this->belongsToMany('App\Company');
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    |
    */




    /*
    |--------------------------------------------------------------------------
    | Functions
    |--------------------------------------------------------------------------
    |
    */

    public function ownedCompanies()
    {
        return $this->hasMany('App\Company');
    }

    public function hasCompany()
    {
        return $this->ownedCompanies()->exists();
    }


    /*
    -------------------------------------------------------------
      The field of expertize of the user.
      Polymorphic relationship since a user can have many domain,
      and a domain can have many user.
    -------------------------------------------------------------
    */
    public function domains()
    {
        return $this->morphMany('App\Domain', 'domainable');
    }

    public function assigned()
    {
        return $this->morphedByMany('App\Project', 'assignable');
    }

    public function ownedTasks()
    {
        return $this->hasMany('App\Task');
    }

    public function tasks()
    {
        return $this->morphedByMany('App\Task', 'assignable');
    }
}
