<?php

namespace App\Events;

use App\Customer;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CustomerBillingEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $customer;
    public $action;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($action, Customer $customer)
    {
        $this->customer = $customer;
        $this->action   = $action; 
    }

}
