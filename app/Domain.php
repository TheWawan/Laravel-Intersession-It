<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = [
      'name', 'domainable_type', 'domainable_id'
    ];

    public function domains()
    {
        return $this->morphTo();
    }
}
