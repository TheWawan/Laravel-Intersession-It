<?php

namespace App\Listeners;

use App\Events\CompanyBillingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CompanyBillingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CompanyBillingEvent  $event
     * @return void
     */
    public function handle(CompanyBillingEvent $event)
    {
      /*
        Update informations of all the bills associated to the
        company.
      */

      $company_name     = $event->customer->name;
      $company_address  = $event->customer->address;
      $company_phone    = $event->customer->phone;
      $company_email    = $event->customer->email;
      $company_siret    = $event->customer->siret;

      foreach($event->company->bills() as $bill)
      {
        $bill->company_name  = $company_name;
        $bill->company_email = $company_email;
        $bill->company_phone = $company_phone;
        $bill->company_email = $company_email;
        $bill->company_siret = $company_siret;
      }
    }
}
