<?php

namespace App\Listeners;

use App\Events\TarriffModifEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PriceRecomputationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TarriffModifEvent  $event
     * @return void
     */
    public function handle(TariffModifEvent $event)
    {
        foreach($event->tariff->company()->bills() as $bill)
        {
          $oldTasksPrice = 0;
          $newTasksPrice = 0;

          foreach(Task::where($bill->project()->tasks()->technical_level, $event->tariff->technical_level) as $task)
          {
              $oldTasksPrice += $event->oldPrice * $task->getNumberOfHours();
              $newTasksPrice += $task->getPrice();
          }

          $bill->total_price += ($newTasksPrice - $oldTasksPrice);
        }
    }
}
