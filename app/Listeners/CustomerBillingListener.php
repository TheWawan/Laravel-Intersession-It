<?php

namespace App\Listeners;

use App\Events\CustomerBillingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerBillingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerBillingEvent  $event
     * @return void
     */
    public function handle(CustomerBillingEvent $event)
    {
      /*
        Update informations of all the bills associated to the
        customer.
      */
        $customer_name  = $customer->name;
        $customer_email = $customer->email;
        $customer_phone = $customer->phone;
        $customer_siret = $customer->siret;

        foreach($event->customer->bills() as $bill)
        {
          $bill->customer_name  = $customer_name;
          $bill->customer_email = $customer_email;
          $bill->customer_phone = $customer_phone;
          $bill->customer_siret = $customer_siret;
        }

    }

}
