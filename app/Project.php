<?php

namespace App;

use App\Traits\Assignable;
use App\Traits\Domainable;
use App\User;
use App\Billing;
use Illuminate\Database\Eloquent\Collection as Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\ModelStatus\HasStatuses;

use App\Bill;

class Project extends Model
{
    use HasStatuses, Assignable, Domainable;

    protected $fillable = [
      'name',
      'description',
      'budget',

      'company_id',
      'user_id'
    ];

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    public function owner()
    {
        return $this->belongsTo('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer');
    }

    public function date()
    {
        return $this->morphOne('App\Date', 'datable');
    }

    public function bill()
    {
      return Billing::where('project_id', $this->id);
    }

}
