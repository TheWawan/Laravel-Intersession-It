<?php

namespace App\Http\Controllers;
use App\Date; 

use Illuminate\Http\Request;

class DateController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $dates = Date::all();

    return response()->json($dates, 200);
  }


  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $date = Date::create($request->all());

    return response()->json($date, 200);
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Date  $date
  * @return \Illuminate\Http\Response
  */
  public function show(Date $date)
  {
    return response()->json($date, 200);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Datable  $date
  * @return \Illuminate\Http\Response
  */
  public function edit(Date $date)
  {
    return response()->json($date, 200);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Datable  $date
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, Date $date)
  {
    $date->update($request->all());

    return response()->json($date, 200);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Datable  $date
  * @return \Illuminate\Http\Response
  */
  public function destroy(Date $date)
  {
    $date->delete();

    return response()->json(null, 204);
  }

}
