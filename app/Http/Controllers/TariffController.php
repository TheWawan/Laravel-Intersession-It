<?php

namespace App\Http\Controllers;

use App\Tariff;
use Illuminate\Http\Request;

class TariffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tariffs = Tariff::all();

        return response()->json($tariffs, 200);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tariff = Tariff::create($request->all());

        return response()->json($tariff, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tariff $tariff
     * @return \Illuminate\Http\Response
     */
    public function show(Tariff $tariff)
    {
        return response()->json($tariff, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tariff $tariff
     * @return \Illuminate\Http\Response
     */
    public function edit(Tariff $tariff)
    {
        return response()->json($tariff, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Tariff              $tariff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tariff $tariff)
    {
        $tariff->update($request->all());

        return response()->json($tariff, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tariff $tariff
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tariff $tariff)
    {
        $tariff->delete();

        return response()->json(null, 204);
    }
}
