<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    protected $fillable = [
      'technical_level',
      'price',

      'company_id'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function scopeForTechnical($query, $technical_level)
    {
        return $query->where('technical_level', $technical_level);
    }
}
