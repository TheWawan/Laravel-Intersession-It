<?php

namespace App\Traits;

/*
|--------------------------------------------------------------------------
| Domainable Trait
|--------------------------------------------------------------------------
|
*/

trait Domainable
{
    public function domains()
    {
        return $this->morphMany('App\Domain', 'domainable');
    }
}
