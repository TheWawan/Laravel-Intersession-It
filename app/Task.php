<?php

namespace App;

use App\Traits\Assignable;
use Illuminate\Database\Eloquent\Model;
use Spatie\ModelStatus\HasStatuses;
use App\User;
use App\Date;

use DateTime;
use DatePeriod;
use DateInterval;

class Task extends Model
{
    use HasStatuses, Assignable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name', 'priority', 'technical_level', 'user_id'
     ];

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function task()
    {
        return $this->belongsTo('App\Task');
    }

    public function risks()
    {
        return $this->hasMany('App\Risk');
    }

    public function undertasks()
    {
        return $this->hasMany('App\Task');
    }

    public function date()
    {
        return $this->morphOne('App\Date', 'datable');
    }

    public function tariffs()
    {
        return $this->project->company->tariffs();
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Utility methods
    |--------------------------------------------------------------------------
    |
    */

    public function getNumberOfHours()
    {
        if ($this->hasDate()) {
            return $this->getWorkDays() * $this->project->company->daily_hours_worked;
        }

        return 0;
    }

    public function scopeForProject($query, $project_id)
    {
        return $query->where('project_id', $project_id);
    }

    public function getPrice()
    {
        if ($this->hasDate()) {
            $workDays = $this->getWorkDays();
            $tariff = $this->tariffs()->forTechnical($this->technical_level)->firstOrFail();
            return $tariff->price * $workDays;
        }

        return 0;
    }

    public function hasDate()
    {
        return $this->date()->exists();
    }

    public function getWorkDays()
    {
        $taskDate = $this->date()->first();


        $start_date = new DateTime($taskDate->start_date);
        $end_date = ($taskDate->end_date == null) ? new DateTime($taskDate->expected_end_date) : new DateTime($taskDate->end_date);

        $interval = $end_date->diff($start_date);
        $nbOfDays = $interval->days;

        $period = new DatePeriod($start_date, new DateInterval('P1D'), $end_date);

        foreach ($period as $date) {
            $currDay = $date->format('D');

            if ($currDay == 'Sat' || $currDay == 'Sun') {
                $nbOfDays--;
            }
        }

        return $nbOfDays;
    }
}
