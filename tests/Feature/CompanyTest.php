<?php

namespace Tests\Feature;

use App\User;
use App\Company;
use App\Tariff;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        Passport::actingAs(factory(User::class)->create());
    }
    /** @test */
    public function display_all_companies()
    {
        $response = $this->get('api/company');

        $response->assertOk()
                ->assertSee(Company::first()->name);
    }

    /** @test */
    public function display_a_company()
    {
        $response = $this->get('api/company/1');

        $response->assertOk()
                ->assertSee(Company::first()->name);
    }

    /** @test */
    public function create_a_company()
    {
        $company = factory('App\Company')->make();

        $response = $this->post('api/company', $company->toArray());
        
        $response->assertOk()->assertSee($company['name']);
    }

    /** @test */
    public function update_a_company()
    {
        $company = factory('App\Company')->create();

        $response = $this->put('api/company/' . $company->id, ['name' => 'NewName']);

        $response->assertOk()
                ->assertSee('NewName');
    }

    /** @test */
    public function delete_a_company()
    {
        $company = factory('App\Company')->create();

        $response = $this->delete('api/company/' . $company->id);

        $response->assertStatus(204);
    }

    /** @test */
    public function add_tariff_to_company()
    {
        $company = factory(Company::class)->create();

        $tariff = factory(Tariff::class)->make();

        $company->tariffs()->create($tariff->toArray());

        $this->assertEquals($company->tariffs()->first()->price, $tariff->price);
    }

    /** @test */
    public function update_tariff_of_company()
    {
        $company = factory(Company::class)->create();

        $tariff = factory(Tariff::class)->make();

        $company->tariffs()->create($tariff->toArray());
        $company->tariffs()->first()->update(['price' => 10]);
        //var_dump($company->tariffs())

        $this->assertEquals($company->tariffs()->first()->price, 10);
    }

}
