<?php

use App\Company;
use App\Domain;
use App\Project;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/*
|--------------------------------------------------------------------------
| MASTER TEST
| This  is  the master test who test
| all the creation of a project
| with all dependancies
|--------------------------------------------------------------------------
|
*/

class MasterTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
    }

    /** @test */
    public function create_a_whole_project()
    {
        $user = factory(User::class)->create(); // Create User

        $company = tap($user->companies(), function ($userCompany) {
            $companyFactory = factory(Company::class)->make();       // Create Company
            $userCompany->create($companyFactory->toArray());        // Store Company
            $userCompany->attach($companyFactory->id);               // Set User Company
        })->first();

        $this->assertEquals($user->companies()->first(), $company);

        $domain = tap($company->domains(), function ($companyDomain) {
            $domainFactory = factory(Domain::class)->make();        // Create Domain
            $companyDomain->create($domainFactory->toArray());      // Store Domain
        })->first();

        $this->assertEquals($company->domains->first(), $domain);

        $project = tap($user->projects(), function ($userProject) {
            $projectFactory = factory(Project::class)->make();      // Create User Project
            $userProject->create($projectFactory->toArray());       // Store User Project
        })->first();

        $this->assertEquals($user->projects->first(), $project);
            $project = tap($project, function ($project) {
            $users = factory(User::class, 6)->create();             // Create Users
            $project->assign($users);                               // Assign users on project
            $this->assertTrue($project->isAssign($users->random()));
        });

        $tasks = factory('App\Task', 10)->make()->each(function ($task) use ($project) {
            $task = $project->tasks()->create($task->toArray());    // Store Task
            $user = factory(User::class)->create();                 // Take a random User
            $task->assign($user);                                   // Assign user to a task
            $this->assertTrue($task->isAssign($user));
        });

        $this->assertEquals($tasks->first()->name, $project->tasks->first()->name);
    }
}
