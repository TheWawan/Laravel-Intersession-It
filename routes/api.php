<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
*/
Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
Route::get('auth/user', 'AuthController@user')->middleware('auth:api');


/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
|
*/
Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('billing', 'BillingController')->except('create');
    Route::resource('user', 'UserController')->except('create', 'store');
    Route::get('users/select', 'UserController@getSelect');

    Route::resource('task', 'TaskController')->except('create');

    Route::resource('company', 'CompanyController')->except('create');

    Route::post('undertask/{task}', 'UnderTaskController@store')->name('undertask.store');
    Route::delete('undertask/{task}', 'UnderTaskController@destroy')->name('undertask.destroy');


    Route::resource('risk', 'RiskController')->except('create');
    Route::resource('tariff', 'TariffController')->except('create');
    Route::resource('customer', 'CustomerController')->except('create');

    Route::resource('project', 'ProjectController')->except('create');
    Route::resource('assign', 'ProjectUserController')->except('create');
    Route::get('tasks/{project}', 'ProjectTaskController@show');

    Route::resource('domain', 'DomainController')->except('create');
    Route::resource('date', 'DateController')->except('create');

    Route::get('/user/search/{name}', 'UserController@search')->name('user.search');
});
